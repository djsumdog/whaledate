# WhaleDate

Keep your docker container up to date by tracking new release tags.

### Example config.yml

```yaml
docker:
  web1:
    url: https://web1.private.example.com:2376
    client_cert: /keys/web1/docker-client.crt
    client_key: /keys/web1/docker-client.pem
    ssl_ca_file: /keys/web1/ca.pem
github:
  mastodon:
    repo: tootsuite/mastodon
  traefik:
    repo: traefik/traefik
```
