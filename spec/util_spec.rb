require_relative '../lib/util'

RSpec.describe Util do

  describe "Ordering versions into hash list" do

    it "extracts only numbered versions" do
      expect(Util.ordered_version_hash(['5.4.3', 'latest'])).to eq({'5.4.3' => ['']})
    end

    it "extracts sub tags with hyphens" do
      expect(Util.ordered_version_hash(['5.4.3-a-b-c', '5.4.3-d-e-f', '4.4.0', '4.4.0-a-b'])).
        to eq({'5.4.3' => ['d-e-f', 'a-b-c'], '4.4.0' => ['a-b', '']})
    end

    it "extracts tags and subtags and orders them correctly" do
      tags = ['latest', '1.3.3', '1.3.4', '1.3.6', '1.3.6-apache',
              '1.3.7-apache', '1.3.7-fpm', '1.3.8-apache', '1.3.8-fpm',
              'elastic', 'latest-apache', 'latest-fpm']
      result = {"1.3.8"=>["fpm", "apache"], "1.3.7"=>["fpm", "apache"], "1.3.6"=>["apache", ""], "1.3.4"=>[""], "1.3.3"=>[""]}
      expect(Util.ordered_version_hash(tags)).to eq(result)
    end

    it "correct ignores non-version tags" do
      tags = ["master", "8.0.0", "7.x", "7.9.4", "7.9.3"]
      result = {"8.0.0"=> [], "7.x"=>[], "7.9.4"=>[], "7.9.3"=>[]}
    end

  end

end
