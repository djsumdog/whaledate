#!/usr/bin/env ruby

require_relative 'lib/dockercheck'
require_relative 'lib/gitcheck.rb'
require_relative 'lib/database.rb'
require 'yaml'
require 'logger'
require 'logger/colors'

$log = Logger.new(STDOUT)

def container_check(client)

  current = client.current
  for container in current
    $log.debug(container)
    clist = container.first
    cname = (clist) ? clist.first : {}
    labels = container.first.last
    if labels.select { |l| l.end_with?(':latest') }.any?
      $log.warn("Container #{cname} using :latest tag. Skipping")
    else
      cur_versions = Util.ordered_version_hash(
        labels.map { |l| l.split(':').last }
      )
      latest_versions = Util.ordered_version_hash(
        client.repo_tags(labels.first.split(':').first)
      )
      $log.debug(cur_versions)
      $log.debug(latest_versions)
      cur = cur_versions.first.first
      latest = latest_versions.first.first
      if cur != latest
        $log.warn("Container update: #{cname}\t Current: #{cur} Latest: #{latest}")
      else
        $log.info("Container #{cname} is up to date: #{cur}")
      end
    end
  end

end

$db = ReleaseDatabase.new($log, '/tmp/check')
config = YAML.load_file('config.yml')

config['github'].each { |name, project|
  $log.info("Checking Project #{project['repo']} ")
  gh = GithubCheck.new($log, $db)
  gh.check(project['repo'])
}

config['docker'].each { |name, box|
  client = DockerCheck.new($log, $db, box['url'], box['client_cert'], box['client_key'], box['ssl_ca_file'])
  $log.info("Checking #{name}")
  client.check()
}
