#!/usr/bin/env ruby
require 'logging'

cs = Logging.color_scheme('example', :logger => [:white, :on_green], :message => :magenta)
pattern = Logging.layouts.pattern(:color_scheme => 'example')
log = Logging.logger(STDOUT)
log.info('Test')
