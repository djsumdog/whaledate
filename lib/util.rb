class Util


  # Orders a list of tags into a dictionary of the version and list of potential subtypes
  # tags without subtypes are represented by the empty string
  def self.ordered_version_hash(version_list)
    version_list.
      select { |x| x =~ /(\d+\.)?(\d+\.)?(\*|\d+)/ }.
      sort.
      reverse.
      each_with_object(Hash.new { |h, k| h[k] = [] }) { |tag, values|
        version, *sub = tag.split('-')
        values[version] << (sub.nil? ? '' : sub.join('-'))
      }
  end

end
