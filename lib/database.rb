require 'sqlite3'

class ReleaseDatabase

  def initialize(logger, db_file)
    @log = logger
    @db = SQLite3::Database.open db_file
    @db.results_as_hash = true
    @db.execute('CREATE TABLE IF NOT EXISTS programs(id INTEGER PRIMARY KEY AUTOINCREMENT, program TEXT, type TEXT)')
    @db.execute('CREATE TABLE IF NOT EXISTS releases(id INTEGER PRIMARY KEY AUTOINCREMENT, program_id INTEGER, release TEXT)')
  end

  def get_program(program, type)
    results = @db.query('SELECT * FROM programs WHERE program=? and type=?', program, type)
    row = results.first
    if row.nil?
      @db.execute('INSERT INTO programs(program, type) VALUES(?, ?)', program, type)
      {"id"=>@db.last_insert_row_id, "program"=>program, "type"=>type}
    else
      row
    end
  end

  def get_releases(program_id)
    @db.query('SELECT * FROM releases WHERE program_id=?', program_id)
  end

  def add_release(program_id, release_version)
    @db.execute('INSERT INTO releases(program_id, release) VALUES(?, ?)', program_id, release_version)
    @db.last_insert_row_id
  end

end