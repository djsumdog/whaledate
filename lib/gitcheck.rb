require 'httparty'
require 'json'

class GithubCheck

  def initialize(logger, database)
    @db = database
    @log = logger
  end

  def check(repo)
    program_meta = @db.get_program(repo, 'github')
    stored_tags = @db.get_releases(program_meta['id'])
    current_releases = repo_releases(repo)

    cur_release_names = current_releases.map { |release| release['tag_name'] }
    prev_releases = stored_tags.map { |row| row['release'] }
    new_releases = cur_release_names - prev_releases
    new_releases.each { |r|
      @log.info("New release for #{repo} :: #{r}")
      @db.add_release(program_meta['id'], r)
    }
  end

  #http https://api.github.com/repos/tootsuite/mastodon/releases
  def repo_releases(repo)
    res = HTTParty.get("https://api.github.com/repos/#{repo}/releases")
    if res.code >= 200 and res.code <= 299
      json = JSON.parse(res.body)
    else
      raise "Could not get Git Tags. Return status #{res.code}"
    end
  end


end
