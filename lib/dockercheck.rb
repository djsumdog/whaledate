require 'docker'
require 'httparty'
require 'json'
require_relative 'util'

class DockerCheck

  def initialize(logger, database, url, client_cert, client_key, ca_file)
    @log = logger
    @db = database
    Docker.url = url
    Docker.options = {
      client_cert: client_cert,
      client_key: client_key,
      ssl_ca_file: ca_file,
      ssl_verify_peer: false,
      scheme: 'https',
      read_timeout: 900
    }
  end

  def current
    Docker::Container.all(:all => true).
      map { |c| [c, Docker::Image.get(c.info['Image'])] }.
      reject { |x| x.last.info['RepoTags'].empty? }.
      map { |y| {y.first.info['Names'].first => y.last.info['RepoTags']} }
  end

  def check()
    daemon_images = current()
    #$log.debug(daemon_images)
    all_images = daemon_images.map { |c| c.values.flatten.first }.uniq
    $log.debug("Images to check #{all_images}")
    images = all_images.reject { |l|
      r = l.end_with?(':latest')
      if r
        $log.warn("Image #{l} using :latest tag. Skipping")
      end
      r
    }

    images.uniq.each { |image_tag|
      image = image_tag.split(':').first
      $log.info("Checking Image #{image}")
      releases = repo_tags(image)

      program_meta = @db.get_program(image, 'docker')
      prev_releases = @db.get_releases(program_meta['id']).map { |row| row['release'] }
      new_releases = releases - prev_releases
      new_releases.each { |r|
        @log.info("New release for #{image} :: #{r}")
        @db.add_release(program_meta['id'], r)
      }
    }
  end

  def remote_repo_check(img_name)
    # Check non-dockerhub V1/V2 repos
    img = img_name.split('/')
    repo_host = img.shift
    img = img.join('/')
    url = "https://#{repo_host}/v2/#{img}/tags/list"
    res = HTTParty.get(url)
    if res.code == 401
      if res.headers.has_key?('www-authenticate')
        auth_type,info = res.headers['www-authenticate'].split(' ')
        auth_req_params = Hash[info.tr('""', '').split(",").map { |str| str.split("=")} ]
        # TODO: check realm, service, scope exist,
        # more error checking below
        res = HTTParty.get(auth_req_params['realm'].to_str, :query => {'service'=>auth_req_params['service'], 'scope'=>auth_req_params['scope']})
        token = JSON.parse(res.body)['token']
        res = HTTParty.get(url, :headers =>  {'Authorization'=>"#{auth_type} #{token}"})
      else
        @log.error("401 Unauthorized with no WWW-Authenticate header for #{url}")
        return {}
      end
    end
    if res.code >= 200 and res.code <= 299
      json = JSON.parse(res.body)
      return json['tags']
    else
      @log.error("Bad Status #{res.code}")
      return {}
    end
  end

  def repo_tags(img_name)
    if img_name.count('/') > 1
      return remote_repo_check(img_name)
    else
      res = HTTParty.get("https://registry.hub.docker.com/v1/repositories/#{img_name}/tags")
      if res.code >= 200 and res.code <= 299
        json = JSON.parse(res.body)
        return json.map { |t| t['name'] }
      else
        @log.error("Error checking #{img_name} in Docker Hub. HTTP Status #{res.code}")
        return {}
      end
    end
  end

end
